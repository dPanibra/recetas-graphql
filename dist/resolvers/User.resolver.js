"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserResolve = void 0;
const type_graphql_1 = require("type-graphql");
const User_1 = require("../entity/User");
const User_input_1 = require("../inputs/User.input");
const isLoged_1 = require("../middleware/isLoged");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
let UserResolve = class UserResolve {
    // SIGNUP
    signup(user) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const userCreate = yield User_1.User.findOne({ where: { email: user.email } });
                if (userCreate)
                    throw new Error("Email already in use");
                if (user.password.length < 5)
                    throw new Error("Password invalid");
                const hashedPassword = yield bcrypt.hash(user.password, 12);
                const newUser = User_1.User.create({
                    email: user.email,
                    password: hashedPassword,
                    name: user.name,
                });
                return yield newUser.save();
            }
            catch (error) {
                console.log(error);
                throw error;
            }
        });
    }
    // LOGIN
    login(email, password) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const user = yield User_1.User.findOne({ email });
                if (!user)
                    throw new Error("Email or Password invalid");
                const isPasswordValid = yield bcrypt.compare(password, user.password);
                if (!isPasswordValid)
                    throw new Error("Email or Password invalid");
                const token = jwt.sign({ email: user.email, user_id: user.id }, "apiGraphQl", {
                    expiresIn: "1d",
                });
                return token;
            }
            catch (error) {
                console.log(error);
                throw error;
            }
        });
    }
    // GET ALL User
    getAllUsers() {
        return User_1.User.find();
    }
    // GET User BY ID
    getOneUser(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const user = yield User_1.User.findOne({ id });
                if (!user)
                    throw new Error("User not found");
                return user;
            }
            catch (error) {
                console.log(error);
                throw error;
            }
        });
    }
    // UPDATE User
    updateUser(id, user) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var myUpdateUser = new User_1.User();
                const userID = yield User_1.User.findOne({ id });
                if (!userID)
                    throw new Error("User not found");
                if (user.name)
                    myUpdateUser.name = user.name;
                if (user.email) {
                    const userEmail = yield User_1.User.findOne({ where: { email: user.email } });
                    if (userEmail)
                        throw new Error("Email already in use");
                    myUpdateUser.email = user.email;
                }
                if (user.password) {
                    if (user.password.length < 5)
                        throw new Error("Password invalid");
                    const hashedPassword = yield bcrypt.hash(user.password, 12);
                    myUpdateUser.password = hashedPassword;
                }
                yield User_1.User.update(id, myUpdateUser);
                const newUserUpdated = yield User_1.User.findOne({ where: { id } });
                return newUserUpdated;
            }
            catch (error) {
                console.log(error);
                throw error;
            }
        });
    }
    // DELETE User
    deleteUser(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const user = yield User_1.User.findOne({ id });
                if (!user)
                    throw new Error("User not found");
                yield User_1.User.delete(id);
                return true;
            }
            catch (error) {
                console.log(error);
                throw error;
            }
        });
    }
};
__decorate([
    type_graphql_1.Mutation(() => User_1.User),
    __param(0, type_graphql_1.Arg("user", () => User_input_1.newUser)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_input_1.newUser]),
    __metadata("design:returntype", Promise)
], UserResolve.prototype, "signup", null);
__decorate([
    type_graphql_1.Mutation(() => String),
    __param(0, type_graphql_1.Arg("email")), __param(1, type_graphql_1.Arg("password")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], UserResolve.prototype, "login", null);
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Query(() => [User_1.User]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], UserResolve.prototype, "getAllUsers", null);
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Query(() => [User_1.User]),
    __param(0, type_graphql_1.Arg("id", () => type_graphql_1.Int)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], UserResolve.prototype, "getOneUser", null);
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Mutation(() => User_1.User),
    __param(0, type_graphql_1.Arg("id", () => type_graphql_1.Int)),
    __param(1, type_graphql_1.Arg("user", () => User_input_1.updateUser)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, User_input_1.updateUser]),
    __metadata("design:returntype", Promise)
], UserResolve.prototype, "updateUser", null);
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Mutation(() => Boolean),
    __param(0, type_graphql_1.Arg("id", () => type_graphql_1.Int)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], UserResolve.prototype, "deleteUser", null);
UserResolve = __decorate([
    type_graphql_1.Resolver()
], UserResolve);
exports.UserResolve = UserResolve;
//# sourceMappingURL=User.resolver.js.map