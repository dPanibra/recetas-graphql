"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecipeResolver = void 0;
const type_graphql_1 = require("type-graphql");
const Recipe_1 = require("../entity/Recipe");
const Category_1 = require("../entity/Category");
const User_1 = require("../entity/User");
const Ingredient_1 = require("../entity/Ingredient");
const Recipe_input_1 = require("../inputs/Recipe.input");
const isLoged_1 = require("../middleware/isLoged");
const jwt = require("jsonwebtoken");
let RecipeResolver = class RecipeResolver {
    // CREATE NEW Recipe
    createRecipe(recipe) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const recipeCreate = yield Recipe_1.Recipe.findOne({
                    where: { name: recipe.name },
                });
                if (recipeCreate)
                    throw new Error("Name already in use");
                const category = yield Category_1.Category.findOne({
                    where: { id: recipe.category_id },
                });
                if (!category)
                    throw new Error("Category not found");
                const user = yield User_1.User.findOne({ where: { id: recipe.user_id } });
                if (!user)
                    throw new Error("User not found");
                const ingredients = yield Ingredient_1.Ingredient.findByIds(recipe.ingredients);
                if (!ingredients)
                    throw new Error("Ingredients not found");
                const newRecipe = Recipe_1.Recipe.create({
                    name: recipe.name,
                    description: recipe.description,
                    user,
                    category,
                    ingredients,
                });
                return yield newRecipe.save();
            }
            catch (error) {
                console.log(error);
                throw error;
            }
        });
    }
    // GET ALL Recipe
    getAllRecipes() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield Recipe_1.Recipe.find({
                relations: ["user", "category", "ingredients"],
            });
        });
    }
    //GET MyRecipes
    getMyRecipes(ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            const user_id = jwt.verify(ctx.req.headers["token"], "apiGraphQl").user_id;
            return yield Recipe_1.Recipe.find({
                where: { user: user_id },
                relations: ["user", "category", "ingredients"],
            });
        });
    }
    // GET Recipe BY ID
    getOneRecipe(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const recipe = yield Recipe_1.Recipe.findOne({ id }, { relations: ["user", "category", "ingredients"] });
                if (recipe) {
                    return recipe;
                }
                throw new Error("Recipe not found");
            }
            catch (error) {
                console.log(error);
                throw error;
            }
        });
    }
    // UPDATE Recipe
    updateRecipe(id, recipe) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var newupdateRecipe = new Recipe_1.Recipe();
                if (recipe.name) {
                    const recipeCreate = yield Recipe_1.Recipe.findOne({
                        where: { name: recipe.name },
                    });
                    if (recipeCreate)
                        throw new Error("Name already in use");
                    newupdateRecipe.name = recipe.name;
                }
                if (recipe.category_id) {
                    const category = yield Category_1.Category.findOne({
                        where: { id: recipe.category_id },
                    });
                    if (!category)
                        throw new Error("Category not found");
                    newupdateRecipe.category = category;
                }
                if (recipe.user_id) {
                    const user = yield User_1.User.findOne({ where: { id: recipe.user_id } });
                    if (!user)
                        throw new Error("User not found");
                    newupdateRecipe.user = user;
                }
                if (recipe.ingredients) {
                    const ingredients = yield Ingredient_1.Ingredient.findByIds(recipe.ingredients);
                    if (!ingredients)
                        throw new Error("Ingredients not found");
                    newupdateRecipe.ingredients = ingredients;
                }
                yield Recipe_1.Recipe.update(id, newupdateRecipe);
                const updaterecipe = yield Recipe_1.Recipe.findOne({ id }, { relations: ["user", "category", "ingredients"] });
                return updaterecipe;
            }
            catch (error) {
                console.log(error);
                throw error;
            }
        });
    }
    // DELETE Recipe
    deleteRecipe(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const recipe = yield Recipe_1.Recipe.findOne({ id });
                if (recipe) {
                    yield Recipe_1.Recipe.delete(id);
                    return true;
                }
                throw new Error("Recipe not found");
            }
            catch (error) {
                console.log(error);
                throw error;
            }
        });
    }
};
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Mutation(() => Recipe_1.Recipe),
    __param(0, type_graphql_1.Arg("recipe", () => Recipe_input_1.newRecipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Recipe_input_1.newRecipe]),
    __metadata("design:returntype", Promise)
], RecipeResolver.prototype, "createRecipe", null);
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Query(() => [Recipe_1.Recipe]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], RecipeResolver.prototype, "getAllRecipes", null);
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Query(() => [Recipe_1.Recipe]),
    __param(0, type_graphql_1.Ctx()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], RecipeResolver.prototype, "getMyRecipes", null);
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Query(() => Recipe_1.Recipe),
    __param(0, type_graphql_1.Arg("id", () => type_graphql_1.Int)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], RecipeResolver.prototype, "getOneRecipe", null);
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Mutation(() => Recipe_1.Recipe),
    __param(0, type_graphql_1.Arg("id", () => type_graphql_1.Int)),
    __param(1, type_graphql_1.Arg("recipe", () => Recipe_input_1.updateRecipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Recipe_input_1.updateRecipe]),
    __metadata("design:returntype", Promise)
], RecipeResolver.prototype, "updateRecipe", null);
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Mutation(() => Boolean),
    __param(0, type_graphql_1.Arg("id", () => type_graphql_1.Int)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], RecipeResolver.prototype, "deleteRecipe", null);
RecipeResolver = __decorate([
    type_graphql_1.Resolver()
], RecipeResolver);
exports.RecipeResolver = RecipeResolver;
//# sourceMappingURL=Recipe.resolver.js.map