"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoryResolver = void 0;
const type_graphql_1 = require("type-graphql");
const Category_1 = require("../entity/Category");
const isLoged_1 = require("../middleware/isLoged");
let CategoryResolver = class CategoryResolver {
    // CREATE NEW Category
    createCategory(name) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const category = yield Category_1.Category.findOne({ name });
                if (category)
                    throw new Error("Name already in use");
                const newCategory = Category_1.Category.create({ name });
                return yield newCategory.save();
            }
            catch (error) {
                console.log(error);
                throw error;
            }
        });
    }
    // GET ALL Category
    getAllCategorys() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield Category_1.Category.find({ relations: ["recipes"] });
        });
    }
    // GET Category BY ID
    getOneCategory(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const category = yield Category_1.Category.findOne({ id }, { relations: ["recipes"] });
                if (!category)
                    throw new Error("Category not found");
                return category;
            }
            catch (error) {
                console.log(error);
                throw error;
            }
        });
    }
    // UPDATE Category
    updateCategory(id, name) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const category = yield Category_1.Category.findOne({ id });
                if (!category)
                    throw new Error("Category not found");
                const categoryName = yield Category_1.Category.findOne({ name });
                if (categoryName)
                    throw new Error("Name already in use");
                yield Category_1.Category.update(id, { name });
                const updataCategory = yield Category_1.Category.findOne({ id });
                return updataCategory;
            }
            catch (error) {
                console.log(error);
                throw error;
            }
        });
    }
    // DELETE Category
    deleteCategory(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const category = yield Category_1.Category.findOne({ id });
                if (!category)
                    throw new Error("Category not found");
                yield Category_1.Category.delete(id);
                return true;
            }
            catch (error) {
                console.log(error);
                throw error;
            }
        });
    }
};
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Mutation(() => Category_1.Category),
    __param(0, type_graphql_1.Arg("name", () => String)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CategoryResolver.prototype, "createCategory", null);
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Query(() => [Category_1.Category]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CategoryResolver.prototype, "getAllCategorys", null);
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Query(() => Category_1.Category),
    __param(0, type_graphql_1.Arg("id", () => type_graphql_1.Int)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], CategoryResolver.prototype, "getOneCategory", null);
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Mutation(() => Category_1.Category),
    __param(0, type_graphql_1.Arg("id", () => type_graphql_1.Int)),
    __param(1, type_graphql_1.Arg("name", () => String)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String]),
    __metadata("design:returntype", Promise)
], CategoryResolver.prototype, "updateCategory", null);
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Mutation(() => Boolean),
    __param(0, type_graphql_1.Arg("id", () => type_graphql_1.Int)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], CategoryResolver.prototype, "deleteCategory", null);
CategoryResolver = __decorate([
    type_graphql_1.Resolver()
], CategoryResolver);
exports.CategoryResolver = CategoryResolver;
//# sourceMappingURL=Category.resolver.js.map