"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IngredientResolver = void 0;
const type_graphql_1 = require("type-graphql");
const Ingredient_1 = require("../entity/Ingredient");
const isLoged_1 = require("../middleware/isLoged");
let IngredientResolver = class IngredientResolver {
    // CREATE NEW Ingredient
    createIngredient(name) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const ingredient = yield Ingredient_1.Ingredient.findOne({ name });
                if (ingredient)
                    throw new Error("Name already in use");
                const newIngredient = Ingredient_1.Ingredient.create({ name });
                return yield newIngredient.save();
            }
            catch (error) {
                console.log(error);
                throw error;
            }
        });
    }
    // GET ALL Ingredient
    getAllIngredients() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield Ingredient_1.Ingredient.find();
        });
    }
    // GET Ingredient BY ID
    getOneIngredient(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const ingredient = yield Ingredient_1.Ingredient.findOne({ id });
                if (!ingredient)
                    throw new Error("Ingredient not found");
                return ingredient;
            }
            catch (error) {
                console.log(error);
                throw error;
            }
        });
    }
    // UPDATE Ingredient
    updateIngredient(id, name) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const ingredient = yield Ingredient_1.Ingredient.findOne({ id });
                if (!ingredient)
                    throw new Error("Ingredient not found");
                const ingredientName = yield Ingredient_1.Ingredient.findOne({ name });
                if (ingredientName)
                    throw new Error("Name already in use");
                yield Ingredient_1.Ingredient.update(id, { name });
                const updataIngredient = yield Ingredient_1.Ingredient.findOne({ id });
                return updataIngredient;
            }
            catch (error) {
                console.log(error);
                throw error;
            }
        });
    }
    // DELETE Ingredient
    deleteIngredient(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const ingredient = yield Ingredient_1.Ingredient.findOne({ id });
                if (!ingredient)
                    throw new Error("Ingredient not found");
                yield Ingredient_1.Ingredient.delete(id);
                return true;
            }
            catch (error) {
                console.log(error);
                throw error;
            }
        });
    }
};
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Mutation(() => Ingredient_1.Ingredient),
    __param(0, type_graphql_1.Arg("name", () => String)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], IngredientResolver.prototype, "createIngredient", null);
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Query(() => [Ingredient_1.Ingredient]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], IngredientResolver.prototype, "getAllIngredients", null);
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Query(() => Ingredient_1.Ingredient),
    __param(0, type_graphql_1.Arg("id", () => type_graphql_1.Int)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], IngredientResolver.prototype, "getOneIngredient", null);
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Mutation(() => Ingredient_1.Ingredient),
    __param(0, type_graphql_1.Arg("id", () => type_graphql_1.Int)),
    __param(1, type_graphql_1.Arg("name", () => String)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String]),
    __metadata("design:returntype", Promise)
], IngredientResolver.prototype, "updateIngredient", null);
__decorate([
    type_graphql_1.UseMiddleware(isLoged_1.isLoged),
    type_graphql_1.Mutation(() => Boolean),
    __param(0, type_graphql_1.Arg("id", () => type_graphql_1.Int)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], IngredientResolver.prototype, "deleteIngredient", null);
IngredientResolver = __decorate([
    type_graphql_1.Resolver()
], IngredientResolver);
exports.IngredientResolver = IngredientResolver;
//# sourceMappingURL=Ingredient.resolver.js.map