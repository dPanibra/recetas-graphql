"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateRecipe = exports.newRecipe = void 0;
const type_graphql_1 = require("type-graphql");
let newRecipe = class newRecipe {
};
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], newRecipe.prototype, "name", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], newRecipe.prototype, "description", void 0);
__decorate([
    type_graphql_1.Field(() => type_graphql_1.Int),
    __metadata("design:type", Number)
], newRecipe.prototype, "user_id", void 0);
__decorate([
    type_graphql_1.Field(() => type_graphql_1.Int),
    __metadata("design:type", Number)
], newRecipe.prototype, "category_id", void 0);
__decorate([
    type_graphql_1.Field(() => [type_graphql_1.Int]),
    __metadata("design:type", Array)
], newRecipe.prototype, "ingredients", void 0);
newRecipe = __decorate([
    type_graphql_1.InputType()
], newRecipe);
exports.newRecipe = newRecipe;
let updateRecipe = class updateRecipe {
};
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], updateRecipe.prototype, "name", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], updateRecipe.prototype, "description", void 0);
__decorate([
    type_graphql_1.Field(() => type_graphql_1.Int, { nullable: true }),
    __metadata("design:type", Number)
], updateRecipe.prototype, "user_id", void 0);
__decorate([
    type_graphql_1.Field(() => type_graphql_1.Int, { nullable: true }),
    __metadata("design:type", Number)
], updateRecipe.prototype, "category_id", void 0);
__decorate([
    type_graphql_1.Field(() => [type_graphql_1.Int], { nullable: true }),
    __metadata("design:type", Array)
], updateRecipe.prototype, "ingredients", void 0);
updateRecipe = __decorate([
    type_graphql_1.InputType()
], updateRecipe);
exports.updateRecipe = updateRecipe;
//# sourceMappingURL=Recipe.input.js.map