"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.startServer = void 0;
const express_1 = __importDefault(require("express"));
const apollo_server_express_1 = require("apollo-server-express");
const type_graphql_1 = require("type-graphql");
const User_resolver_1 = require("./resolvers/User.resolver");
const Category_resolver_1 = require("./resolvers/Category.resolver");
const Recipe_resolver_1 = require("./resolvers/Recipe.resolver");
const Ingredient_resolver_1 = require("./resolvers/Ingredient.resolver");
// const redis = require("redis");
// const session = require("express-session");
// let RedisStore = require("connect-redis")(session);
// let redisClient = redis.createClient();
function startServer() {
    return __awaiter(this, void 0, void 0, function* () {
        const app = express_1.default();
        // app.use(
        //   session({
        //     secret: "grapRecipe",
        //     name: "graphlqRecipes",
        //     resave: false,
        //     saveUninitialized: false,
        //   })
        // );
        const serve = new apollo_server_express_1.ApolloServer({
            playground: true,
            introspection: true,
            schema: yield type_graphql_1.buildSchema({
                resolvers: [
                    User_resolver_1.UserResolve,
                    Category_resolver_1.CategoryResolver,
                    Recipe_resolver_1.RecipeResolver,
                    Ingredient_resolver_1.IngredientResolver,
                ],
            }),
            context: ({ req, res }) => ({ req, res }),
        });
        serve.applyMiddleware({ app, path: "/" });
        return app;
    });
}
exports.startServer = startServer;
//# sourceMappingURL=app.js.map