"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.isLoged = void 0;
const jwt = require("jsonwebtoken");
exports.isLoged = ({ context }, next) => __awaiter(void 0, void 0, void 0, function* () {
    const token = context.req.headers["token"];
    if (token) {
        try {
            var decoded = jwt.verify(token, "apiGraphQl");
            if (decoded.email) {
                return next();
            }
            throw new Error("Access Denied! Please login to continue");
        }
        catch (error) {
            console.log(error);
        }
    }
    throw new Error("Access Denied! Please login to continue");
});
//# sourceMappingURL=isLoged.js.map