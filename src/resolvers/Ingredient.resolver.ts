import {
  Resolver,
  Query,
  Mutation,
  Arg,
  Int,
  UseMiddleware,
} from "type-graphql";
import { Ingredient } from "../entity/Ingredient";

import { isLoged } from "../middleware/isLoged";
@Resolver()
export class IngredientResolver {
  // CREATE NEW Ingredient
  @UseMiddleware(isLoged)
  @Mutation(() => Ingredient)
  async createIngredient(@Arg("name", () => String) name: string) {
    try {
      const ingredient = await Ingredient.findOne({ name });
      if (ingredient) throw new Error("Name already in use");

      const newIngredient = Ingredient.create({ name });
      return await newIngredient.save();

    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  // GET ALL Ingredient
  @UseMiddleware(isLoged)
  @Query(() => [Ingredient])
  async getAllIngredients() {
    return await Ingredient.find();
  }

  // GET Ingredient BY ID
  @UseMiddleware(isLoged)
  @Query(() => Ingredient)
  async getOneIngredient(@Arg("id", () => Int) id: number) {
    try {
      const ingredient = await Ingredient.findOne({ id });
      if (!ingredient) throw new Error("Ingredient not found");

      return ingredient;

    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  // UPDATE Ingredient
  @UseMiddleware(isLoged)
  @Mutation(() => Ingredient)
  async updateIngredient(
    @Arg("id", () => Int) id: number,
    @Arg("name", () => String) name: string
  ) {
    try {
      const ingredient = await Ingredient.findOne({ id });
      if (!ingredient) throw new Error("Ingredient not found");

      const ingredientName = await Ingredient.findOne({ name });
      if (ingredientName) throw new Error("Name already in use");

      await Ingredient.update(id, { name });
      const updataIngredient = await Ingredient.findOne({ id });
      return updataIngredient;

    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  // DELETE Ingredient
  @UseMiddleware(isLoged)
  @Mutation(() => Boolean)
  async deleteIngredient(@Arg("id", () => Int) id: number) {
    try {
      const ingredient = await Ingredient.findOne({ id });
      if (!ingredient) throw new Error("Ingredient not found");

      await Ingredient.delete(id);
      return true;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}
