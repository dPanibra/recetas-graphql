import {
  Resolver,
  Query,
  Mutation,
  Arg,
  Int,
  UseMiddleware,
} from "type-graphql";

import { User } from "../entity/User";
import { newUser, updateUser } from "../inputs/User.input";
import { isLoged } from "../middleware/isLoged";

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

@Resolver()
export class UserResolve {
  // SIGNUP
  @Mutation(() => User)
  async signup(@Arg("user", () => newUser) user: newUser) {
    try {
      const userCreate = await User.findOne({ where: { email: user.email } });
      if (userCreate) throw new Error("Email already in use");
      if (user.password.length < 5) throw new Error("Password invalid");
      const hashedPassword = await bcrypt.hash(user.password, 12);
      const newUser = User.create({
        email: user.email,
        password: hashedPassword,
        name: user.name,
      });
      return await newUser.save();
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  // LOGIN
  @Mutation(() => String)
  async login(@Arg("email") email: string, @Arg("password") password: string) {
    try {
      const user = await User.findOne({ email });
      if (!user) throw new Error("Email or Password invalid");

      const isPasswordValid = await bcrypt.compare(password, user.password);
      if (!isPasswordValid) throw new Error("Email or Password invalid");

      const token = jwt.sign(
        { email: user.email, user_id: user.id },
        "apiGraphQl",
        {
          expiresIn: "1d",
        }
      );
      return token;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  // GET ALL User
  @UseMiddleware(isLoged)
  @Query(() => [User])
  getAllUsers() {
    return User.find();
  }
  // GET User BY ID
  @UseMiddleware(isLoged)
  @Query(() => [User])
  async getOneUser(@Arg("id", () => Int) id: number) {
    try {
      const user = await User.findOne({ id });
      if (!user) throw new Error("User not found");
      return user;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
  // UPDATE User
  @UseMiddleware(isLoged)
  @Mutation(() => User)
  async updateUser(
    @Arg("id", () => Int) id: number,
    @Arg("user", () => updateUser) user: updateUser
  ) {
    try {
      var myUpdateUser = new User();
      const userID = await User.findOne({ id });
      if (!userID) throw new Error("User not found");

      if (user.name) myUpdateUser.name = user.name;
      if (user.email) {
        const userEmail = await User.findOne({ where: { email: user.email } });
        if (userEmail) throw new Error("Email already in use");
        myUpdateUser.email = user.email;
      }
      if (user.password) {
        if (user.password.length < 5) throw new Error("Password invalid");
        const hashedPassword = await bcrypt.hash(user.password, 12);
        myUpdateUser.password = hashedPassword;
      }
      await User.update(id, myUpdateUser);
      const newUserUpdated = await User.findOne({ where: { id } });
      return newUserUpdated;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  // DELETE User
  @UseMiddleware(isLoged)
  @Mutation(() => Boolean)
  async deleteUser(@Arg("id", () => Int) id: number) {
    try {
      const user = await User.findOne({ id });
      if (!user) throw new Error("User not found");

      await User.delete(id);
      return true;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}
