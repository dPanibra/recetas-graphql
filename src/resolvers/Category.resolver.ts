import {
  Resolver,
  Query,
  Mutation,
  Arg,
  Int,
  UseMiddleware,
} from "type-graphql";
import { Category } from "../entity/Category";
import { isLoged } from "../middleware/isLoged";

@Resolver()
export class CategoryResolver {
  // CREATE NEW Category
  @UseMiddleware(isLoged)
  @Mutation(() => Category)
  async createCategory(@Arg("name", () => String) name: string) {
    try {
      const category = await Category.findOne({ name });
      if (category) throw new Error("Name already in use");

      const newCategory = Category.create({ name });
      return await newCategory.save();
      
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  // GET ALL Category
  @UseMiddleware(isLoged)
  @Query(() => [Category])
  async getAllCategorys() {
    return await Category.find({ relations: ["recipes"] });
  }

  // GET Category BY ID
  @UseMiddleware(isLoged)
  @Query(() => Category)
  async getOneCategory(@Arg("id", () => Int) id: number) {
    try {
      const category = await Category.findOne( { id }, { relations: ["recipes"] } );
      if (!category) throw new Error("Category not found");

      return category;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  // UPDATE Category
  @UseMiddleware(isLoged)
  @Mutation(() => Category)
  async updateCategory(
    @Arg("id", () => Int) id: number,
    @Arg("name", () => String) name: string
  ) {
    try {
      const category = await Category.findOne({ id });
      if (!category) throw new Error("Category not found");

      const categoryName = await Category.findOne({ name });
      if (categoryName) throw new Error("Name already in use");

      await Category.update(id, { name });
      const updataCategory = await Category.findOne({ id });

      return updataCategory;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  // DELETE Category
  @UseMiddleware(isLoged)
  @Mutation(() => Boolean)
  async deleteCategory(@Arg("id", () => Int) id: number) {
    try {
      const category = await Category.findOne({ id });
      if (!category) throw new Error("Category not found");

      await Category.delete(id);
      return true;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}
