import {
  Resolver,
  Query,
  Mutation,
  Arg,
  Int,
  Ctx,
  UseMiddleware,
} from "type-graphql";
import { getConnection } from "typeorm";
import { Recipe } from "../entity/Recipe";
import { Category } from "../entity/Category";
import { User } from "../entity/User";
import { Ingredient } from "../entity/Ingredient";
import { newRecipe, updateRecipe } from "../inputs/Recipe.input";

import { isLoged } from "../middleware/isLoged";
import { MyContext } from "../types/MyContext";

const jwt = require("jsonwebtoken");

@Resolver()
export class RecipeResolver {
  // CREATE NEW Recipe
  @UseMiddleware(isLoged)
  @Mutation(() => Recipe)
  async createRecipe(@Arg("recipe", () => newRecipe) recipe: newRecipe) {
    try {
      const recipeCreate = await Recipe.findOne({
        where: { name: recipe.name },
      });
      if (recipeCreate) throw new Error("Name already in use");

      const category = await Category.findOne({
        where: { id: recipe.category_id },
      });
      if (!category) throw new Error("Category not found");

      const user = await User.findOne({ where: { id: recipe.user_id } });
      if (!user) throw new Error("User not found");

      const ingredients = await Ingredient.findByIds(recipe.ingredients);
      if (!ingredients) throw new Error("Ingredients not found");

      const newRecipe = Recipe.create({
        name: recipe.name,
        description: recipe.description,
        user,
        category,
        ingredients,
      });
      return await newRecipe.save();
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  // GET ALL Recipe
  @UseMiddleware(isLoged)
  @Query(() => [Recipe])
  async getAllRecipes() {
    return await Recipe.find({
      relations: ["user", "category", "ingredients"],
    });
  }

  //GET MyRecipes
  @UseMiddleware(isLoged)
  @Query(() => [Recipe])
  async getMyRecipes(@Ctx() ctx: MyContext) {
    const user_id = jwt.verify(ctx.req.headers["token"], "apiGraphQl").user_id;
    return await Recipe.find({
      where: {user:user_id},
      relations: ["user", "category", "ingredients"],
    });
  }

  // GET Recipe BY ID
  @UseMiddleware(isLoged)
  @Query(() => Recipe)
  async getOneRecipe(@Arg("id", () => Int) id: number) {
    try {
      const recipe = await Recipe.findOne(
        { id },
        { relations: ["user", "category", "ingredients"] }
      );
      if (recipe) {
        return recipe;
      }
      throw new Error("Recipe not found");
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  // UPDATE Recipe
  @UseMiddleware(isLoged)
  @Mutation(() => Recipe)
  async updateRecipe(
    @Arg("id", () => Int) id: number,
    @Arg("recipe", () => updateRecipe) recipe: updateRecipe
  ) {
    try {
      var newupdateRecipe = new Recipe();
      if (recipe.name) {
        const recipeCreate = await Recipe.findOne({
          where: { name: recipe.name },
        });
        if (recipeCreate) throw new Error("Name already in use");
        newupdateRecipe.name = recipe.name;
      }

      if (recipe.category_id) {
        const category = await Category.findOne({
          where: { id: recipe.category_id },
        });
        if (!category) throw new Error("Category not found");
        newupdateRecipe.category = category;
      }

      if (recipe.user_id) {
        const user = await User.findOne({ where: { id: recipe.user_id } });
        if (!user) throw new Error("User not found");
        newupdateRecipe.user = user;
      }

      if (recipe.ingredients) {
        const ingredients = await Ingredient.findByIds(recipe.ingredients!);
        if (!ingredients) throw new Error("Ingredients not found");
        newupdateRecipe.ingredients = ingredients;
      }
      await Recipe.update(id, newupdateRecipe);

      const updaterecipe = await Recipe.findOne(
        { id },
        { relations: ["user", "category", "ingredients"] }
      );
      return updaterecipe;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  // DELETE Recipe
  @UseMiddleware(isLoged)
  @Mutation(() => Boolean)
  async deleteRecipe(@Arg("id", () => Int) id: number) {
    try {
      const recipe = await Recipe.findOne({ id });
      if (recipe) {
        await Recipe.delete(id);
        return true;
      }
      throw new Error("Recipe not found");
    } catch (error) {
      console.log(error);
      throw error;
    }
  }
}
