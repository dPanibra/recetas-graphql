import express from "express";
import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";

import { UserResolve } from "./resolvers/User.resolver";
import { CategoryResolver } from "./resolvers/Category.resolver";
import { RecipeResolver } from "./resolvers/Recipe.resolver";
import { IngredientResolver } from "./resolvers/Ingredient.resolver";
// const redis = require("redis");
// const session = require("express-session");

// let RedisStore = require("connect-redis")(session);
// let redisClient = redis.createClient();

export async function startServer() {
  const app = express();
  // app.use(
  //   session({
  //     secret: "grapRecipe",
  //     name: "graphlqRecipes",
  //     resave: false,
  //     saveUninitialized: false,
  //   })
  // );
  const serve = new ApolloServer({
    playground: true,
    introspection: true,
    schema: await buildSchema({
      resolvers: [
        UserResolve,
        CategoryResolver,
        RecipeResolver,
        IngredientResolver,
      ],
    }),

    context: ({ req, res }) => ({ req, res }),
  });

  serve.applyMiddleware({ app, path: "/" });

  return app;
}
