import "reflect-metadata";
import dotenv from 'dotenv';
import { connect } from "./config/typeorm";
import { startServer } from "./app";
dotenv.config();
async function main() {
  connect();
  const app = await startServer();
  app.listen(process.env.PORT || 3000, () => {
    console.log('🏃 Running Server');
  });
}

main();
