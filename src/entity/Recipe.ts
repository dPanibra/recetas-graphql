import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  BaseEntity,
  ManyToOne,
  ManyToMany, JoinColumn
} from "typeorm";
import { Field, ObjectType } from "type-graphql";

import { User } from "./User";
import { Category } from "./Category";
import { Ingredient } from "./Ingredient";

@ObjectType()
@Entity()
export class Recipe extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
  id!: number;

  @Field()
  @Column({ type: "varchar", length: 50, unique: true })
  name!: string;

  @Field()
  @Column({ type: "varchar", length: 1024, unique: false })
  description!: string;

  @Field(() => String)
  @CreateDateColumn({ type: "timestamp" })
  createdAt!: string;

  @UpdateDateColumn({ type: "timestamp" })
  updateAt!: string;

  @Field(() => User)
  @ManyToOne((type) => User, (user) => user.recipes)
  @JoinColumn({name:'user_id'})
  user!: User;

  @Field(() => Category)
  @ManyToOne((type) => Category, (category) => category.recipes)
  @JoinColumn({name:'category_id'})
  category!: Category;

  @Field(() => [Ingredient])
  @ManyToMany((type) => Ingredient, (ingredient) => ingredient.recipes,{eager:true, cascade:true})
  ingredients!: Ingredient[];
}
