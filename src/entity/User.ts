import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  BaseEntity,
  OneToMany,
} from "typeorm";
import { Field, ObjectType } from "type-graphql";
import { Recipe } from "./Recipe";

@ObjectType()
@Entity()
export class User extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
  id!: number;

  @Field()
  @Column({ type: "varchar", length: 50, unique: false })
  name!: string;

  @Field()
  @Column({ type: "varchar", length: 50, unique: true })
  email!: string;

  @Field()
  @Column()
  password!: string;

  @Field(() => String)
  @CreateDateColumn({ type: "timestamp" })
  createdAt!: string;

  @UpdateDateColumn({ type: "timestamp" })
  updatedAt!: string;

  @Field(() => [Recipe])
  @OneToMany((type) => Recipe, (recipe) => recipe.user)
  recipes!: Recipe[];
}
