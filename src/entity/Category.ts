import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  BaseEntity,
  OneToMany,
} from "typeorm";
import { Field, ObjectType } from "type-graphql";
import { Recipe } from "./Recipe";
@ObjectType()
@Entity()
export class Category extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
  id!: number;

  @Field()
  @Column({ type: "varchar", length: 50, unique: true })
  name!: string;

  @Field(() => String)
  @CreateDateColumn({ type: "timestamp" })
  createdAt!: string;

  @Field(() => String)
  @UpdateDateColumn({ type: "timestamp" })
  updatedAt!: string;

  @Field(() => [Recipe])
  @OneToMany((type) => Recipe, (recipe) => recipe.category, {
    eager: true,
    cascade: true,
  })
  recipes!: Recipe[];
}
