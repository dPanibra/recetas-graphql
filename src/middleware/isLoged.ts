import { MiddlewareFn } from "type-graphql";
import { MyContext } from "../types/MyContext";
const jwt = require("jsonwebtoken");

export const isLoged: MiddlewareFn<MyContext> = async ({ context }, next) => {
  const token = context.req.headers["token"];
  if (token) {
    try {
      var decoded = jwt.verify(token, "apiGraphQl");
      if (decoded.email) {
        return next();
      }
      throw new Error("Access Denied! Please login to continue");
    } catch (error) {
      console.log(error);
    }
  }
  throw new Error("Access Denied! Please login to continue");
};
