import { InputType, Field, Int } from "type-graphql";

@InputType()
export class newRecipe {
  @Field()
  name!: string;

  @Field()
  description!: string;

  @Field(() => Int)
  user_id!: number;

  @Field(() => Int)
  category_id!: number;

  @Field(() => [Int])
  ingredients!: number[];
}

@InputType()
export class updateRecipe {
  @Field({ nullable: true })
  name?: string;

  @Field({ nullable: true })
  description?: string;

  @Field(() => Int, { nullable: true })
  user_id?: number;

  @Field(() => Int, { nullable: true })
  category_id?: number;

  @Field(() => [Int], { nullable: true })
  ingredients?: number[];
}
